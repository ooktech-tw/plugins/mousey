title: $:/plugins/OokTech/Mousey/A note about popups

There are two ways to make popups:

!! Popups anywhere on the wiki

These popups use the `$action-mouseposition` widget to get the position.
The popups themselves are tiddlers that are tagged with `$:/tags/PageTemplate`
and css is used to position it on the screen.

This lets you put a div anywhere, but it isn't inside a tiddler and doesn't
move with the story river.

They are turned on and off by adding or removing the `$:/tags/PageTemplate`
tag.

!! Popups inside a `$mouse` widget

Popups in a `$mouse` widget are tiddlers that are transcluded inside the
`$mouse` widget.
To get the popup to work they have special css classes that set the position
and visibility of the popup.

The general structure is:

```
<div
  class=popupClass
>
  //popup content here//
</div>
```

and the popup class is:

```
.popupClass {
  display: {{$:/state/PopupState!!display}};
  top: {{$:/state/PopupState!!y}};
  left: {{$:/state/PopupState!!x}};
  overflow: visible;
  height:0px;
  width:0px;
}
```

You have the `$mouse` widget store the mouse position in the `$:/state/
PopupState` tiddler.
Then you have whatever you want triggering your popup toggle the value in the
`display` field of the `$:/state/PopupState` between `visible` and `none`.

Then the popup has to be transcluded at the top of the `$mouse` widget. If you
have anything above the transclusion(s) the popups may not be positioned
correctly.