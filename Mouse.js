/*\
title: $:/plugins/OokTech/Mousey/mouse.js
type: application/javascript
module-type: widget

Mouse actions widget

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

var Widget = require("$:/core/modules/widgets/widget.js").widget;

var MouseWidget = function(parseTreeNode,options) {
	this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
MouseWidget.prototype = new Widget();

/*
Render this widget into the DOM
*/
MouseWidget.prototype.render = function(parent,nextSibling) {
	var self = this;
	// Remember parent
	this.parentDomNode = parent;
	// Compute attributes and execute state
	this.computeAttributes();
	this.execute();
	var tag = this.parseTreeNode.isBlock ? "div" : "span";
	if(this.tag && $tw.config.htmlUnsafeElements.indexOf(this.tag) === -1) {
		tag = this.tag;
	}
	// Create element
	var domNode = this.document.createElement(tag);
	// Assign classes
	var classes = (this["class"] || "").split(" ");
	classes.push("tc-mouse");
	domNode.className = classes.join(" ");
	// Add the mouse event handlers
	if (self.tiddler) {
		setInterval(
			function () {
				if (self.oldX !== self.x || self.oldY !== self.y) {
					const tid = $tw.wiki.getTiddler(self.tiddler);
					const existing = (typeof tid !== 'undefined') ? tid.fields : {title: self.tiddler};
					const fields = {}
					fields[self.xField] = self.x;
					fields[self.yField] = self.y;
					$tw.wiki.addTiddler(new $tw.Tiddler(existing, fields));
					self.oldX = self.x;
					self.oldY = self.y;
				}
			}
		, 100)
		domNode.addEventListener("mousemove", function(event) {
			const rect = domNode.getBoundingClientRect();
			self.x = event.clientX - rect.left;
			self.y = event.clientY - rect.top;
		});
	}
	domNode.addEventListener("mousedown", function (event) {
		if (self.mousedown && event.button == 0) {
			self.invokeActions(self,event);
			if(self.mousedown) {
				self.invokeActionString(self.mousedown,self,event);
			}
			self.dispatchMessage(event);
			event.preventDefault();
			event.stopPropagation();
			return true;
		}
		if (self.rightmousedown && event.button == 2) {
			self.invokeActions(self,event);
			if(self.rightmousedown) {
				self.invokeActionString(self.rightmousedown,self,event);
			}
			self.dispatchMessage(event);
			event.preventDefault();
			event.stopPropagation();
			return true;
		}
	},false);
	domNode.addEventListener("mouseup", function (event) {
		if (self.mouseup && event.button == 0) {
			self.invokeActions(self,event);
			self.invokeActionString(self.mouseup,self,event);
			self.dispatchMessage(event);
			event.preventDefault();
			event.stopPropagation();
			return true;
		}
		if (self.rightmouseup && event.button == 2) {
			self.invokeActions(self,event);
			self.invokeActionString(self.rightmouseup,self,event);
			self.dispatchMessage(event);
			event.preventDefault();
			event.stopPropagation();
			return true;
		}
	},false);
	domNode.addEventListener("click", function (event) {
		if (self.click && event.button == 0) {
			self.invokeActions(self,event);
			self.invokeActionString(self.click,self,event);
			self.dispatchMessage(event);
			event.preventDefault();
			event.stopPropagation();
			return true;
		}
		if (self.rightclick && event.button == 2) {
			self.invokeActions(self,event);
			self.invokeActionString(self.rightclick,self,event);
			self.dispatchMessage(event);
			event.preventDefault();
			event.stopPropagation();
			return true;
		}
	},false);
	domNode.addEventListener("contextmenu", function (event) {
		if (self.rightclick || self.rightmouseup || self.rightmousedown || self.rightdblclick) {
			event.preventDefault();
		}
		if (self.rightclick) {
			self.invokeActions(self,event);
			self.invokeActionString(self.rightclick,self,event);
			self.dispatchMessage(event);
			event.preventDefault();
			event.stopPropagation();
			return true;
		}
	},false);
	domNode.addEventListener("dblclick", function (event) {
		if (self.dblclick && event.button == 0) {
			self.invokeActions(self,event);
			self.invokeActionString(self.dblclick,self,event);
			self.dispatchMessage(event);
			event.preventDefault();
			event.stopPropagation();
			return true;
		}
		if (this.rightdblclick && event.button == 2) {
			self.invokeActions(self,event);
			self.invokeActionString(self.dblclick,self,event);
			self.dispatchMessage(event);
			event.preventDefault();
			event.stopPropagation();
			return true;
		}
	},false);
	//domNode.addEventListener("mouseenter", function (event) {
	domNode.addEventListener("mouseover", function (event) {
		if (self.mouseenter) {
			self.invokeActions(self,event);
			self.invokeActionString(self.mouseenter,self,event);
			self.dispatchMessage(event);
			event.preventDefault();
			event.stopPropagation();
			return true;
		}
	},false);
	//domNode.addEventListener("mouseleave", function (event) {
	domNode.addEventListener("mouseout", function (event) {
		if(self.mouseleave) {
			self.invokeActions(self,event);
			self.invokeActionString(self.mouseleave,self,event);
			self.dispatchMessage(event);
			event.preventDefault();
			event.stopPropagation();
			return true;
		}
	},false);
	// Insert element
	parent.insertBefore(domNode,nextSibling);
	this.renderChildren(domNode,null);
	this.domNodes.push(domNode);
};

MouseWidget.prototype.dispatchMessage = function(event) {
	this.dispatchEvent({type: this.message, param: this.param, tiddlerTitle: this.getVariable("currentTiddler")});
};

/*
Compute the internal state of the widget
*/
MouseWidget.prototype.execute = function() {
	var self = this;
	// Get attributes
	this.click = this.getAttribute("click","");
	this.dblclick = this.getAttribute("dblclick","");
	this.mousedown = this.getAttribute("down", "");
	this.mouseup = this.getAttribute("up", "");
	this.rightclick = this.getAttribute("rightclick","");
	this.rightdblclick = this.getAttribute("rightdblclick", "");
	this.rightmousedown = this.getAttribute("rightdown","");
	this.rightmouseup = this.getAttribute("rightup","");
	this.mouseenter = this.getAttribute("enter", "");
	this.mouseleave = this.getAttribute("leave", "");
	this.message = this.getAttribute("message","");
	this.param = this.getAttribute("param","");
	this.tag = this.getAttribute("tag","");
	this["class"] = this.getAttribute("class","");
	this.tiddler = this.getAttribute("tiddler", "");
	this.xField = this.getAttribute("xfield", "x");
	this.yField = this.getAttribute("yfield", "y");
	this.x = 0;
	this.y = 0;
	// Make child widgets
	this.makeChildWidgets();
};

/*
Selectively refreshes the widget if needed. Returns true if the widget or any of its children needed re-rendering
*/
MouseWidget.prototype.refresh = function(changedTiddlers) {
	var changedAttributes = this.computeAttributes();
	if(changedAttributes.message || changedAttributes.param || changedAttributes.key || changedAttributes["class"] || changedAttributes.tag) {
		this.refreshSelf();
		return true;
	}
	// Update the keyInfoArray if one of its shortcut-config-tiddlers has changed
	if(this.shortcutTiddlers && $tw.utils.hopArray(changedTiddlers,this.shortcutTiddlers)) {
		this.keyInfoArray = $tw.keyboardManager.parseKeyDescriptors(this.key);
	}
	return this.refreshChildren(changedTiddlers);
};

exports.mouse = MouseWidget;

})();
