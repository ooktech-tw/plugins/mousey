title: $:/plugins/OokTech/Mousey/examples

\define clickExample()
<$mouse
  click="<$action-setfield $tiddler='$:/state/MouseyExample' action='click' text=<<clickExample>>/>"
  class=click
  tag=td
>
  <b>
    Click Here
  </b>
  <br>
  this activates when you click inside it
</$mouse>
\end

\define dblclickExample()
<$mouse
  dblclick="<$action-setfield $tiddler='$:/state/MouseyExample' action='double click' text=<<dblclickExample>>/>"
  class=click
  tag=td
>
  <b>
    Double Click Here
  </b>
  <br>
  this activates when you double click inside it
</$mouse>
\end

\define mouseDownExample()
<$mouse
  down="<$action-setfield $tiddler='$:/state/MouseyExample' action='mouse down' text=<<mouseDownExample>>/>"
  class=click
  tag=td
>
  <b>
    Mouse Down Here
  </b>
  <br>
  this activates if the mouse button is pressed while the cursor is inside it.
</$mouse>
\end

\define mouseUpExample()
<$mouse
  up="<$action-setfield $tiddler='$:/state/MouseyExample' action='mouse up' text=<<mouseUpExample>>/>"
  class=click
  tag=td
>
  <b>
    Mouse Up Here
  </b>
  <br>
  this activates if the mouse button is released while the cursor is inside it.
</$mouse>
\end

\define mouseEnterExample()
<$mouse
  enter="<$action-setfield $tiddler='$:/state/MouseyExample' action='enter' text=<<mouseEnterExample>>/>"
  class=click
  tag=td
>
  <b>
    Enter this
  </b>
  <br>
  this activates if the mouse cursor enters it
</$mouse>
\end

\define mouseLeaveExample()
<$mouse
  leave="<$action-setfield $tiddler='$:/state/MouseyExample' action='leave' text=<<mouseLeaveExample>>/>"
  class=click
  tag=td
>
  <b>
    Leave this
  </b>
  <br>
  this activates if the mouse cursor leaves it
</$mouse>
\end

\define rightClickExample()
<$mouse
  rightclick="<$action-setfield $tiddler='$:/state/MouseyExample' action='right click' text=<<rightClickExample>>/>"
  class=click
  tag=td
>
  <b>
    Right Click Here
  </b>
  <br>
  this activates if the right mouse button is clicked while the cursor is inside it.
</$mouse>
\end

\define rightDblClickExample()
<$mouse
  rightdblclick="<$action-setfield $tiddler='$:/state/MouseyExample' action='right double click' text=<<rightDblClickExample>>/>"
  class=click
  tag=td
>
  <b>
    Right Double Click
  </b>
  <br>
  <!--this activates if the right mouse button is double clicked while the cursor is inside it.-->
  This doesn't work, it is just listed here for completeness.
</$mouse>
\end

\define rightMouseDownExample()
<$mouse
  rightdown="<$action-setfield $tiddler='$:/state/MouseyExample' action='right mouse down' text=<<rightMouseDownExample>>/>"
  class=click
  tag=td
>
  <b>
    Right Mouse Down Here
  </b>
  <br>
  this activates if the right mouse button is pressed while the cursor is inside it.
</$mouse>
\end

\define rightMouseUpExample()
<$mouse
  rightup="<$action-setfield $tiddler='$:/state/MouseyExample' action='right mouse up' text=<<rightMouseUpExample>>/>"
  class=click
  tag=td
>
  <b>
    Right Mouse Up Here
  </b>
  <br>
  this activates if the right mouse button is released while the cursor is inside it.
</$mouse>
\end

<style>
.click {
  background-color: #ddd;
  padding: 10px;
  text-align: center;
  border: solid;
}

.mousePositionDiv {
  background-color: #ddd;
  border: solid;
  width: 100%;
  height: 20em;
}

.mousePositionDiv2 {
  background-color: #ddd;
  border: solid;
  width: 100%;
  height: 20em;
  top: {{$:/state/ExamplePopup!!y}};
  left: {{$:/state/ExamplePopup!!x}};
}

.mouseHoverDiv {
  background-color: #eee;
  border: solid;
  height: 5em;
  top: {{$:/state/ExamplePopup!!y}};
  left: {{$:/state/ExamplePopup!!x}};
  position: absolute;
}
</style>

!! Mouse actions

Do the action listed in each cell and below the code for that cell will be
displayed.

<table style='width:100%;'>
  <tr>
    <<clickExample>>
    <<dblclickExample>>
    <<mouseDownExample>>
    <<mouseUpExample>>
  </tr>
  <tr>
    <<rightClickExample>>
    <<rightDblClickExample>>
    <<rightMouseDownExample>>
    <<rightMouseUpExample>>
  </tr>
  <tr>
    <<mouseEnterExample>>
    <td></td>
    <td></td>
    <<mouseLeaveExample>>
  </tr>
</table>


!!! Most recently activated action and the code it uses

<div
  style = 'border:solid 1px;background-color:#ddd;'
>

<div
  style='text-align:center;font-size:30px;text-align:center;width:100%;'
>
  {{$:/state/MouseyExample!!action}}
</div>

<pre>
  <$view
    tiddler='$:/state/MouseyExample'
    field='text'
  />
</pre>

</div>

---

!! Cursor position - measured globally

This uses the `$mouse` widget to set click and right click actions and it uses
the `$action-mouseposition` widget to place the popup at the mouse cursor.

Right click in the box below to make a popup appear, click outside the popup to
make it go away.

The `$action-mouseposition` widget can give the position anywhere on the page,
but it is only updated when you activate the action widget. You can use buttons
or anything else that can trigger an action widget.

The code is shown below.

<$mouse
  tag=div
  rightclick="<$action-mouseposition/><$action-setfield $tiddler='$:/plugins/OokTech/Mousey/popuptiddler' tags='$:/tags/PageTemplate'/>"
  click="<$action-setfield $tiddler='$:/plugins/OokTech/Mousey/popuptiddler' tags=''/>"
  class=mousePositionDiv
>

Right click X position: {{$:/state/mouseposition!!x}}
<br>
Right click Y position: {{$:/state/mouseposition!!y}}

</$mouse>

Code:

```
<$mouse
  tag=div
  rightclick="<$action-mouseposition/><$action-setfield $tiddler='$:/plugins/OokTech/Mousey/popuptiddler' tags='$:/tags/PageTemplate'/>"
  click="<$action-setfield $tiddler='$:/plugins/OokTech/Mousey/popuptiddler' tags=''/>"
  class=mousePositionDiv
>

Right click X position: {{$:/state/mouseposition!!x}}
<br>
Right click Y position: {{$:/state/mouseposition!!y}}

</$mouse>
```

!! Cursor position - measured inside the $mouse widget

Right click to make the popup appear, left click to make it go away.

<$mouse
  tag=div
  rightclick="<$action-setfield $tiddler='$:/state/MouseyPopup' display='block' static_x={{$:/state/MouseyPopup!!x}} static_y={{$:/state/MouseyPopup!!y}}/>"
  click="<$action-setfield $tiddler='$:/state/MouseyPopup' display=none/>"
  class=mousePositionDiv2
  tiddler='$:/state/MouseyPopup'
>
{{$:/plugins/OokTech/Mousey/popuptiddler3}}
{{$:/plugins/OokTech/Mousey/popuptiddler2}}

Right click X position: {{$:/state/MouseyPopup!!x}}
<br>
Right click Y position: {{$:/state/MouseyPopup!!y}}

</$mouse>

! Drawbacks

Positions using the `$action-mouseposition` don't move with tiddlers, and the
`$action-mouseposition` widget doesn't work in tiddlers opened in separate
windows, but it can give a position anywhere on the page.

Positions using the `$mouse` widget are only available in content wrapped in
the `$mouse` widget, but they work in tiddlers opened in separate windows.