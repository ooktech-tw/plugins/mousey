/*\
title: $:/plugins/OokTech/Mousey/action-getmouseposition.js
type: application/javascript
module-type: widget

Action widget that updates the mouse position when it fires

\*/
(function(){

/*jslint node: true, browser: true */
/*global $tw: false */
"use strict";

const Widget = require("$:/core/modules/widgets/widget.js").widget;

const ActionMousePosition = function(parseTreeNode,options) {
  this.initialise(parseTreeNode,options);
};

/*
Inherit from the base widget class
*/
ActionMousePosition.prototype = new Widget();

/*
Render this widget into the DOM
*/
ActionMousePosition.prototype.render = function(parent,nextSibling) {
  this.computeAttributes();
  this.execute();
};

/*
Compute the internal state of the widget
*/
ActionMousePosition.prototype.execute = function() {
  this.tiddler = this.getAttribute('tiddler', '$:/state/mouseposition')
  this.xfield = this.getAttribute('xfield', 'x')
  this.yfield = this.getAttribute('yfield', 'y')
};

/*
Refresh the widget by ensuring our attributes are up to date
*/
ActionMousePosition.prototype.refresh = function(changedTiddlers) {
  const changedAttributes = this.computeAttributes();
  if(Object.keys(changedAttributes).length) {
    this.refreshSelf();
    return true;
  }
  return this.refreshChildren(changedTiddlers);
};

/*
Invoke the action associated with this widget
*/
ActionMousePosition.prototype.invokeAction = function(triggeringWidget,event) {
  const fields = {"title": this.tiddler};
  fields[this.xfield] = $tw.mouseX;
  fields[this.yfield] = $tw.mouseY;
  $tw.wiki.addTiddler(new $tw.Tiddler(fields));
};

exports["action-mouseposition"] = ActionMousePosition;

})();
