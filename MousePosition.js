/*\
title: $:/plugins/OokTech/Mousey/MousePosition.js
type: application/javascript
module-type: startup

this records the mouse position every 100ms and saves it to window.mouseX and
window.mouseY so other things can use it.
\*/
(function () {

  /*jslint node: true, browser: true */
  /*global $tw: false */
  "use strict";

  // Export name and synchronous status
  exports.name = "mouse-position";
  exports.platforms = ["browser"];
  exports.after = ["render"];
  exports.synchronous = true;

  if ($tw.browser) {
    setTimeout(function () {
      this.document.addEventListener("mousemove", function (e) {
        $tw.mouseX = e.clientX + window.scrollX;
        $tw.mouseY = e.clientY + window.scrollY;
      })
    }, 1000);
  }
})();
